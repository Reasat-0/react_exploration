import React, {Component} from 'react';
import Card from './Cards/Card';
import './App.css'


class App extends Component{

    state = {
        players : [
            { id: "1" , name : "Ronaldo" , age : 31},
            { id: "2" , name : "Messi" , age : 32},
            { id: "3" , name : "Zlatan" , age : 33},
            { id: "4" , name : "Salah" , age : 34},
        ],
        showPlayersards : false
    }

    toggle_cards = () => {
        const show_cards = !this.state.showPlayersards;
        this.setState({
            showPlayersards : show_cards
        })
    }

    deleteCard = (idx) => {
        const cards = [...this.state.players];
              cards.splice(idx,1)

        this.setState({
            players : cards
        })
    }
    inputChangeAge = (e,id) =>{
        const players = {...this.state.players[id]}

        players.age = e.target.value;

        const playersArr = [...this.state.players];

        playersArr[id] = players;


        this.setState({ players : playersArr })
    }

    render(){

        let Cards = null;


        if(this.state.showPlayersards){
            Cards = (
                
                <div className="card_holder">
                    {
                        this.state.players.map((p,i) => {
                            return (
                                <Card 
                                    name={p.name}
                                    age={p.age}
                                    clickCross= {this.deleteCard.bind(this,i)}
                                    inputChange= {(e) => this.inputChangeAge(e,i)}
                                    key={p.id}
                                />
                            )
                        })
                    }

                </div>
                
            )
        }


        return(
            
            <div className="App custom_app">
                <div className="button_holder">
                    <button className="show_hide_button" onClick={this.toggle_cards}> Show Or Hide Cards </button>
                </div>
                { Cards }
            
            </div>
            
        )
    }
}


export default App;