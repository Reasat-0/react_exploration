import React from 'react';

const Person = (props) => {
    return (
        <div>
            <p> This is {props.name} from person component and his age is { props.children } </p>
            <input type="text" onChange={props.change} value={props.name}/>
            <button onClick={props.click}> Delete </button>
        </div>
    )
}

export default Person;