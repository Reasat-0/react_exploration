import React from 'react';
import '../App.css'


const Card = (props) => {
    return (
        <div className = "card">
            <h2> Player Name : {props.name }</h2>
            <p> His age is <b> { props.age } </b> years </p>
            <input type="number" onChange={props.inputChange}></input>
            <button className="cross" onClick={props.clickCross}>X</button>
        </div>
    )
}

export default Card;
