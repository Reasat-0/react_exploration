import React, { Component,  useState } from 'react';
import './App.css';
import Person from './Person/Person';
import Card from './Cards/Card';


class App extends Component{
  state = {
    persons : [
      { id: "i1", name: "Maxiii", age: 21},
      { id: "i2", name: "Fax", age: 22},
      { id: "i3", name: "Tax", age: 23}
    ],
    otherPersons : ["Hola"]
  }
  // nameChangeHandler = (newName,id) => {
  //   this.setState({
  //     persons : [
  //       { name: newName}
        
  //     ],
  //     showPersons : false
  //   })
  // }

  inputChangeHandler = (e,id) => {
    const personIdx = this.state.persons.findIndex((p)=> {
      return p.id === id;
    })
    const person = { ...this.state.persons[personIdx] }
    person.name = e.target.value;

    const persons = [...this.state.persons];

    persons[personIdx] = person;

    this.setState({ persons : persons })
  }
  deletePerson = (i) => {
    const persons = [...this.state.persons];
    persons.splice(i,1)
    this.setState({ persons })
  }
  togglePersons = () => {
    const doesShow = !this.state.showPersons;
    this.setState({showPersons : doesShow})

  }

  render(){


    const styles = {
      background: 'green',
      color: 'white',
      padding: '10px'
      
    }
    let persons = null;

    
    if(this.state.showPersons){
      persons = (
        <div>

          {
            
            this.state.persons.map((person,index) => {
             return <Person 
                            name={person.name}
                            age= {person.age}
                            click={ () => this.deletePerson (index)}
                            key= {person.id}
                            change={(event) => this.inputChangeHandler(event,person.id)}
                    />
            })
          }
 
        </div>
      );

      styles.background  = 'red';
    }

    const color_classes = []
    if(this.state.persons.length < 2){
      color_classes.push("red","bold")
    }
    else if(this.state.persons.length < 3){
      color_classes.push("red")
    }


    return (
      <div className="App">
        <h1> Hello World </h1>

        <p className={ color_classes.join(" ")}> Color Indicator </p>

        {/* <button onClick={this.nameChangeHandler.bind(this,'Ronaldo')}> Change Name</button> */}
        <button style={styles} onClick={this.togglePersons}> Change Name</button>
        {persons}
      </div>
    )
  }
}

// function App(){

//   const [ playersState , setPlayersState ] = useState({
//     player : [
//       { name : "Zlatan" , age : 35 },
//       { name : "Salah" , age : 28},
//       { name : "De Bruyne" , age : 30},
//       { name : "Messi",  age : 32}
//     ]
//   })

//   const clickHandler = () => {
//     setPlayersState({
//       player : [
//         { name : "Larsen" , age : 31 },
//         { name : "Firminho" , age : 32},
//         { name : "Hazzard" , age : 33},
//         { name : "De Maria",  age : 34}
//       ]
//     })
//   }



//   return (
//     <div className="App">
      
//       <div className="card_container">
//         <Card name={ playersState.player[0].name} age={ playersState.player[0].age }></Card>
//         <Card name={ playersState.player[1].name} age={ playersState.player[1].age }></Card>
//         <Card name={ playersState.player[2].name} age={ playersState.player[2].age }></Card>
//         <Card name={ playersState.player[3].name} age={ playersState.player[3].age }></Card>
//       </div>


//       <button onClick = { clickHandler }> Change Players Name </button>


//     </div>
//   )
// }






// function App() {

//   const [ personState , setPersonState] = useState({
//     person : [
//       {name : "Hola" , age: 21 }
//     ]
//   })

//   const clickHandler = () => {
//     setPersonState({
//       person : [
//         {name : "Bolaaa" , age: 22 }
//       ]
//     })
//   }
  


//   return (
//     <div className="App">
//       <h1> Hello World </h1>

//       <button onClick={ clickHandler }> Change Name </button>

//       <Person name={ personState.person[0].name }> { personState.person[0].age }</Person>
//       <Person name="Choka"> Love to play football </Person>
//       <Person/>
//     </div>
//   );
// }

export default App;
